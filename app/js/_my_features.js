'use strict';

(function() {



// // прижатий футер
// $(document).ready(function(){
//   var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
//   var footer = $('footer')[0];
//   var footerElHeightFromTop = footer.offsetTop;
//   var footerHeight = footer.clientHeight
  

//   if ((h - footerHeight) > footerElHeightFromTop) {
//     var a = h - footerHeight;
//     $(footer).offset({top: a});

//     console.dir(footer)
//     console.log('lower')
//   }
// });
  


//кастомний чекбокс
$(document).ready(function(){
    
    $(".chkbx-target").click(function(){
        if ($('input.enrolment-form_chk').is(':checked')) {
          $(".enrolment-form_my-chk").addClass('active');
        } else {
          $(".enrolment-form_my-chk").removeClass('active');
        }
    });
 
});


    //акордеон на сторінці адміна

$(document).ready(function () {
  
  $('.admin-district-list_item').click(function (e) {
      
      if (e.target == this) {
        if ($(this).find(".district-card").hasClass('hidden')) {
          $(".district-card").addClass('hidden');
          $(this).find(".district-card").toggleClass('hidden');        
        } else {
          $(this).find(".district-card").addClass('hidden')
        }
      }
      
    })
})


// функція для валідації полів форми
$(function(){
  $("#mainform").validate({
  rules: {
    idCode: "required",
    idName: "required",
    contact_person: "required",
    phone: {
      required: true
    },
    email: {
      required: true,
      email: true
    },
    approvalChekbox: "required"
  },
  errorElement: "p",
  onfocusout: function(element) { $(element).valid(); },
  focusInvalid: true,
  messages: {
      idCode: "Поле обов'язкове для заповнення",
      idName: "Поле обов'язкове для заповнення",
      contact_person: {
        required: "Поле обов'язкове для заповнення"
      },
      phone: "Поле обов'язкове для заповнення",
      email: "Поле обов'язкове для заповнення",
      approvalChekbox: "Потрібно надати згоду на обробку персональних даних"
  }
  });

  $("#login-form").validate({
    rules: {
      login: "required",
      password: "required"
    },
    messages: {
      login: "Потрібно вказати логін",
      password: "Потрібно вказати пароль"
    },
    errorElement: "p",
    submitHandler: function(e) {
      // e.preventDefault();
      // console.log(e)

      var requestObj = {
        login: $('[name="login"]').val(),
        password: $('[name="password"]').val()
      }

      console.dir(JSON.stringify(requestObj));

      $.ajax({
        url: "/login-form-handler", // указываем URL и
        method: 'POST',
        dataType: "json", // тип загружаемых данных
        data: JSON.stringify(requestObj),
        success: function (data) {
          // вешаем свой обработчик события success
          alert('succesful')
        }
      });
    }
  });

$("#login-admin").validate({
    rules: {
      login: "required",
      password: "required"
    },
    messages: {
      login: "Потрібно вказати логін",
      password: "Потрібно вказати пароль"
    },
    errorElement: "p",
    submitHandler: function(e) {
      // e.preventDefault();
      // console.log(e)

      var requestObj = {
        login: $('[name="login"]').val(),
        password: $('[name="password"]').val()
      }

      console.dir(JSON.stringify(requestObj));


      //перенести в колбек аякса


      $.ajax({
        url: "/login-form-handler", // указываем URL и
        method: 'POST',
        dataType: "json", // тип загружаемых данных
        data: JSON.stringify(requestObj),
        success: function (data) {
          // вешаем свой обработчик события success
          alert('succesful')

            $('.content-area').empty();
            var adminDistricts = _.template(
              '<% for (var a in obj) { %>'+
                '<li class="admin-district-list_item"> <%= obj[a].name %>'+
                  '<% for (var b in obj[a].vpr) { %>'+
                    '<ul class="district-card hidden">'+
                        '<li class="district-card_item"> <%= obj[a].vpr[b].name %> </li>'+
                        '<li class="district-card_item"> <%= obj[a].vpr[b].adress %> </li>'+
                        '<li class="district-card_item"> <%= obj[a].vpr[b].phone %> </li>'+
                        '<p>Активний</p><input type="checkbox" <% if (obj[a].vpr[b].status == true) { %>  <%= "checked" %> <%}%> >'+
                    '</ul> '+
                  '<% } %>'+
                '</li>'+
              '<% } %>'    
          )

          $('.content-area').append(adminDistricts(data));
        }
      });
    }

  });

});





// лічильник в формі "кількість ключів"
$(document).ready(function(){
  let counter = 1;

  $('.clicker').val(counter);
  $(".btn-up").click(function(){
      if (counter < 15) {
        ++counter
        $('.clicker').val(counter);
      } else {
        $('.clicker').val(counter);
      }
  });

  $(".btn-down").click(function(){
      if (counter <= 1) {
        counter = 1
        $('.clicker').val(counter);
      } else {
        --counter;
        $('.clicker').val(counter);
      }
  });
});


// маска вводу в формі
$(document).ready(function(){
  $('#phone').inputmask("+38(999)-99-99-999");  //static mask 
  $('#email').inputmask("*{2,20}@*{2,20}.*{2,20}");  //static mask 
});


})();













