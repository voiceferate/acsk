$(document).ready( function() {
  
  var vprCard = _.template(
    '<% for (var c in obj[localStorage.a].vpr) { %>'+
        '<div class="vpr-card"><%= obj[localStorage.a].vpr[c].name %></div>'+
        '<div class="vpr-card_adress"><%= obj[localStorage.a].vpr[c].adress %></div>'+
        '<div class="vpr-card_phone"><%= obj[localStorage.a].vpr[c].phone %></div>'+
        '<div class="vpr-card_worktime"><%= obj[localStorage.a].vpr[c].worktime %></div>'+
    '<% } %>'
  );
  
  
  var adminDistricts = _.template(
      '<% for (var a in obj) { %>'+
        '<li class="admin-district-list_item"> <%= obj[a].name %>'+
          '<% for (var b in obj[a].vpr) { %>'+
            '<ul class="district-card hidden">'+
                '<li class="district-card_item"> <%= obj[a].vpr[b].name %> </li>'+
                '<li class="district-card_item"> <%= obj[a].vpr[b].adress %> </li>'+
                '<li class="district-card_item"> <%= obj[a].vpr[b].phone %> </li>'+
                '<p>Активний</p><input type="checkbox" <% if (obj[a].vpr[b].status == true) { %>  <%= "checked" %> <%}%> >'+
            '</ul> '+
          '<% } %>'+
        '</li>'+
      '<% } %>'    
  )

  var formDistr = _.template(
      '<select>'+
        '<option value="<%=MAPOBJ[localStorage.a].name%>" > <%=MAPOBJ[localStorage.a].name%> </option>'+
      '</select>'
    );

  // $('.admin-district-list').append(adminDistricts(MAPOBJ));
  // // console.log($('.enrolment-form_field').first())
  $('.enrolment-form_field').first().append(formDistr(MAPOBJ));
  $('.vpr-chose').append(vprCard(MAPOBJ));

  // $('.my-content').append(compiled(MAPOBJ));

});





var MAPOBJ = {
    "0" : {
        "name" : "м. Київ",
        "vpr" : { "vpr1" : {  
                    "name" : "Голосіївський район",
                    "status" : true,
                    "adress" : "м. Київ, вул. Жилянська, буд. 23а, каб. 104",
                    "phone" : "(044) 591-61-30",
                    "worktime" : "Реєстрація користувачів: пн - чт з 9:00 до 18:00пт з 9:00 до 16:45 обідня перерва: з 13:00 до 13:45",
                    "map_link" : "https://www.google.com.ua/maps/place/%D0%90%D0%A6%D0%A1%D0%9A+%D0%86%D0%94%D0%94+%D0%94%D0%A4%D0%A1/@50.4338527,30.5098886,17.08z/data=!4m8!1m2!2m1!1z0JDQptCh0Jog0IbQlNCUINCU0KTQoQ!3m4!1s0x40d4cee3339de817:0x79bce4b53d78206b!8m2!3d50.4337914!4d30.5119267?hl=uk"
                  },
                  "vpr2" : {  
                    "name" : "Дарницький район",
                    "status" : true,
                    "adress" : "м. Київ, вул. Кошиця, буд. 3, 1-й пов.",
                    "phone" : "(044) 596-60-05",
                    "worktime" : "Реєстрація користувачів: пн - чт з 9:00 до 18:00пт з 9:00 до 16:45 обідня перерва: з 13:00 до 13:45",
                    "map_link" : "https://www.google.com.ua/maps/place/%D0%90%D0%A6%D0%A1%D0%9A+%D0%86%D0%94%D0%94+%D0%94%D0%A4%D0%A1/@50.4048457,30.6451808,17.17z/data=!4m8!1m2!2m1!1z0JDQptCh0Jog0IbQlNCUINCU0KTQoQ!3m4!1s0x40d4c4559fb40ce9:0xe53a534028debbd0!8m2!3d50.4049965!4d30.6466964?hl=uk"
                  },
                  "vpr3" : {  
                    "name" : "Дніпровський район",
                    "status" : true,
                    "adress" : "м. Київ, Харківське шосе, буд. 4а, 1-й поверх праворуч",
                    "phone" : "(044) 296-75-66",
                    "worktime" : "Реєстрація користувачів: пн - чт з 9:00 до 18:00пт з 9:00 до 16:45 обідня перерва: з 13:00 до 13:45",
                    "map_link" : "https://www.google.com.ua/maps/place/%D0%90%D0%A6%D0%A1%D0%9A+%D0%86%D0%94%D0%94+%D0%94%D0%A4%D0%A1/@50.4397846,30.6248601,17.21z/data=!4m8!1m2!2m1!1z0JDQptCh0Jog0IbQlNCUINCU0KTQoQ!3m4!1s0x40d4c5436963ffff:0xbdfced1ccd7fc8ea!8m2!3d50.4394762!4d30.6268676?hl=uk"
                  },
                  "vpr4" : {  
                    "name" : "Оболонський район",
                    "status" : true,
                    "adress" : "м. Київ, пр-т. Героїв Сталінграда, буд. 58, 1-й пов., каб. 130",
                    "phone" : "(044) 464-05-27",
                    "worktime" : "Реєстрація користувачів: пн - чт з 9:00 до 18:00пт з 9:00 до 16:45 обідня перерва: з 13:00 до 13:45",
                    "map_link" : "https://www.google.com.ua/maps/place/%D0%90%D0%A6%D0%A1%D0%9A+%D0%86%D0%94%D0%94+%D0%94%D0%A4%D0%A1/@50.5255167,30.5152591,17z/data=!4m8!1m2!2m1!1z0JDQptCh0Jog0IbQlNCUINCU0KTQoQ!3m4!1s0x40d4d23710deca27:0x47d67450f1a40516!8m2!3d50.525613!4d30.5172279?hl=uk"
                  },
                  "vpr5" : {  
                    "name" : "Печерський район",
                    "status" : true,
                    "adress" : "м. Київ, вул. Лескова, буд. 4, 1-й поверх ліворуч",
                    "phone" : "(044) 201-89-03",
                    "worktime" : "Реєстрація користувачів: пн - чт з 9:00 до 18:00пт з 9:00 до 16:45 обідня перерва: з 13:00 до 13:45",
                    "map_link" : "https://www.google.com.ua/maps/place/%D0%90%D0%A6%D0%A1%D0%9A+%D0%86%D0%94%D0%94+%D0%94%D0%A4%D0%A1/@50.4308617,30.5399079,17.08z/data=!4m8!1m2!2m1!1z0JDQptCh0Jog0IbQlNCUINCU0KTQoQ!3m4!1s0x40d4cf0f2a19282b:0xd52cec809f9e45e9!8m2!3d50.4312175!4d30.5399755?hl=uk"
                  },
                  "vpr6" : {  
                    "name" : "Святошинський район",
                    "status" : true,
                    "adress" : "м. Київ, вул. Котельникова, буд. 14, 1-й пов., каб. 100",
                    "phone" : "(044) 452-35-36",
                    "worktime" : "Реєстрація користувачів: пн - чт з 9:00 до 18:00пт з 9:00 до 16:45 обідня перерва: з 13:00 до 13:45",
                    "map_link" : "https://www.google.com.ua/maps/place/%D0%90%D0%A6%D0%A1%D0%9A+%D0%86%D0%94%D0%94+%D0%94%D0%A4%D0%A1/@50.4524054,30.38142,17.12z/data=!4m8!1m2!2m1!1z0JDQptCh0Jog0IbQlNCUINCU0KTQoQ!3m4!1s0x40d4cc89e7750f5d:0x7870c435efd13d23!8m2!3d50.452754!4d30.3830206?hl=uk"
                  },
                  "vpr7" : {  
                    "name" : "Шевченківський район",
                    "status" : true,
                    "adress" : "м. Київ, пров. Політехнічний, буд. 5а, каб. 8",
                    "phone" : "(044) 238-02-29",
                    "worktime" : "Реєстрація користувачів: пн - чт з 9:00 до 18:00пт з 9:00 до 16:45 обідня перерва: з 13:00 до 13:45",
                    "map_link" : "https://www.google.com.ua/maps/place/%D0%90%D0%A6%D0%A1%D0%9A+%D0%86%D0%94%D0%94+%D0%94%D0%A4%D0%A1/@50.447823,30.4665951,17.21z/data=!4m8!1m2!2m1!1z0JDQptCh0Jog0IbQlNCUINCU0KTQoQ!3m4!1s0x40d4ce84bc14d147:0xd788960a3c255f91!8m2!3d50.4477582!4d30.4681036?hl=uk"
                  },
      },
    },
    "1" : {
        "name" : "Вінницька область",
        "vpr" : { "vpr1" : {  "name" : "м. Вінниця",
                              "status" : true,
                              "adress" : "м. Вінниця, вул. Костянтина Василенка, буд. 21, 1-й пов., зал №1 ЦОПП",
                              "phone" : "(0432) 56-04-03",
                              "worktime" : "Реєстрація користувачів: пн - чт з 9:00 до 18:00пт з 9:00 до 16:45 обідня перерва: з 13:00 до 13:45",
                              "map_link" : "https://www.google.com.ua/maps/place/%D0%90%D0%A6%D0%A1%D0%9A+%D0%86%D0%94%D0%94+%D0%94%D0%A4%D0%A1/@50.447823,30.4665951,17.21z/data=!4m8!1m2!2m1!1z0JDQptCh0Jog0IbQlNCUINCU0KTQoQ!3m4!1s0x40d4ce84bc14d147:0xd788960a3c255f91!8m2!3d50.4477582!4d30.4681036?hl=uk"
                            }
          },
      },
    "2" : {
        "name" : "Волинська область",
        "vpr" : { "vpr1" : {  "name" : "м. Луцьк",
                              "status" : true,
                              "adress" : "м. Луцьк, Київський майдан, буд. 4, каб. 30",
                              "phone" : "(0332) 77-84-94",
                              "worktime" : "Реєстрація користувачів: пн - чт з 9:00 до 18:00пт з 9:00 до 16:45 обідня перерва: з 13:00 до 13:45",
                              "map_link" : "https://www.google.com.ua/maps/place/%D0%90%D0%A6%D0%A1%D0%9A+%D0%86%D0%94%D0%94+%D0%94%D0%A4%D0%A1/@50.7430193,25.3542669,17.21z/data=!4m8!1m2!2m1!1z0JDQptCh0Jog0IbQlNCUINCU0KTQoQ!3m4!1s0x0:0x9a221add802bad15!8m2!3d50.7434077!4d25.3554153?hl=uk"
                            },
                  "vpr2" : {  "name" : "м. Ковель",
                              "status" : true,
                              "adress" : "м. Ковель, вул. Чубинського, буд. 1, каб. 105",
                              "phone" : "(03352) 59817",
                              "worktime" : "Реєстрація користувачів: пн - чт з 9:00 до 18:00пт з 9:00 до 16:45 обідня перерва: з 13:00 до 13:45",
                              "map_link" : "https://www.google.com.ua/maps/place/%D0%90%D0%A6%D0%A1%D0%9A+%D0%86%D0%94%D0%94+%D0%94%D0%A4%D0%A1/@51.2145818,24.7153892,17.17z/data=!4m8!1m2!2m1!1z0JDQptCh0Jog0IbQlNCUINCU0KTQoQ!3m4!1s0x0:0x60285bfa3fec9e14!8m2!3d51.2157552!4d24.7158909?hl=uk"
                            }
        },
    },
    "3" : {
        "name" : "Дніпропетровська область",
        "vpr" : { "vpr1" : {  "name" : "м. Дніпро",
                              "status" : true,
                              "adress" : "м. Дніпро, просп. Слобожанський, буд. 95а, ЦОПП, зал 3, каб. 9,10",
                              "phone" : "(0562) 273076",
                              "worktime" : "Реєстрація користувачів: пн - чт з 9:00 до 18:00пт з 9:00 до 16:45 обідня перерва: з 13:00 до 13:45",
                              "map_link" : "https://www.google.com.ua/maps/place/%D0%90%D0%A6%D0%A1%D0%9A+%D0%86%D0%94%D0%94+%D0%94%D0%A4%D0%A1/@48.5136937,35.0755352,17.17z/data=!4m8!1m2!2m1!1z0JDQptCh0Jog0IbQlNCUINCU0KTQoSDQtNC90ZbQv9GA0L4!3m4!1s0x40d9585ff433f14f:0x41fd811023de6646!8m2!3d48.5144535!4d35.0768485?hl=uk"
                            },
                  "vpr2" : {  "name" : "м. Дніпро",
                              "status" : true,
                              "adress" : "м. Дніпро, просп. Богдана Хмельницького, буд. 25, вікна реєстрації користувачів ЕЦП №21, №22",
                              "phone" : "(056) 749-63-34",
                              "worktime" : "Реєстрація користувачів: пн - чт з 9:00 до 18:00пт з 9:00 до 16:45 обідня перерва: з 13:00 до 13:45",
                              "map_link" : "https://www.google.com.ua/maps/place/%D0%90%D0%A6%D0%A1%D0%9A+%D0%86%D0%94%D0%94+%D0%94%D0%A4%D0%A1/@48.4392954,35.019643,17.08z/data=!4m8!1m2!2m1!1z0JDQptCh0Jog0IbQlNCUINCU0KTQoSDQtNC90ZbQv9GA0L4!3m4!1s0x40dbe347266eadb5:0x946276db24f960ce!8m2!3d48.4387962!4d35.0221559?hl=uk"
                            },
                  "vpr3" : {  "name" : "м. Кривий Ріг",
                              "status" : true,
                              "adress" : "м. Кривий Ріг, просп. Героїв-підпільників, буд. 42, каб. 112а",
                              "phone" : "(056) 404-06-95",
                              "worktime" : "Реєстрація користувачів: пн - чт з 9:00 до 18:00пт з 9:00 до 16:45 обідня перерва: з 13:00 до 13:45",
                              "map_link" : "https://www.google.com.ua/maps/place/%D0%90%D0%A6%D0%A1%D0%9A+%D0%86%D0%94%D0%94+%D0%94%D0%A4%D0%A1/@47.9113481,33.3785129,17z/data=!4m12!1m6!3m5!1s0x40db20a14d5f154b:0xe30a06a2e249bfa6!2z0JDQptCh0Jog0IbQlNCUINCU0KTQoQ!8m2!3d47.911546!4d33.3804259!3m4!1s0x40db20a14d5f154b:0xe30a06a2e249bfa6!8m2!3d47.911546!4d33.3804259?hl=uk"
                            },
        },
    },
    "4" : {
        "name" : "Донецька область",
        "vpr" : { "vpr1" : {  "name" : "м. Краматорськ",
                              "status" : true,
                              "adress" : "м. Краматорськ, бульв. Машинобудівників, буд. 22 зал №3",
                              "phone" : "(06264) 74387",
                              "worktime" : "Реєстрація користувачів: пн - чт з 9:00 до 18:00пт з 9:00 до 16:45 обідня перерва: з 13:00 до 13:45",
                              "map_link" : "https://www.google.com.ua/maps/place/%D0%90%D0%A6%D0%A1%D0%9A+%D0%86%D0%94%D0%94+%D0%94%D0%A4%D0%A1/@48.7369035,37.5759394,17z/data=!4m8!1m2!2m1!1z0LDRhtGB0Log0ZbQtNC0INC00YTRgSDQtNC-0L3QtdGG0YzQutCwINC-0LHQu9Cw0YHRgtGMIA!3m4!1s0x40df9705e83cb77f:0x5d63a8e1a43aa8fc!8m2!3d48.737798!4d37.5778997?hl=uk"
                            },
                  "vpr2" : {  "name" : "м. Маріуполь",
                              "status" : true,
                              "adress" : "м. Маріуполь, просп. Металургів, буд. 193, каб. 212",
                              "phone" : "(0629) 384539",
                              "worktime" : "Реєстрація користувачів: пн - чт з 9:00 до 18:00пт з 9:00 до 16:45 обідня перерва: з 13:00 до 13:45",
                              "map_link" : "https://www.google.com.ua/maps/place/%D0%90%D0%A6%D0%A1%D0%9A+%D0%86%D0%94%D0%94+%D0%94%D0%A4%D0%A1/@47.12779,37.5639757,17.12z/data=!4m8!1m2!2m1!1z0LDRhtGB0Log0ZbQtNC0INC00YTRgSDQtNC-0L3QtdGG0YzQutCwINC-0LHQu9Cw0YHRgtGMIA!3m4!1s0x40e6e1551832bcbf:0xbf078a2fdfad37c5!8m2!3d47.128104!4d37.5656595?hl=uk"
                            },
        },
    },
    "5" : {
        "name" : "Житомирська область"
    },
    "6" : {
        "name" : "Закарпатська область"
    },
    "7" : {
        "name" : "Запорізька область"
    },
    "8" : {
        "name" : "Івано-Франківська область"
    },
    "9" : {
        "name" : "Київська область"
    },
    "10" : {
        "name" : "Кіровоградська область"
    },
    "11" : {
        "name" : "Луганська область"
    },
    "12" : {
        "name" : "Львівська область"
    },
    "13" : {
        "name" : "Миколаївська область"
    },
    "14" : {
        "name" : "Одеська область"
    },
    "15" : {
        "name" : "Полтавська область"
    },
    "16" : {
        "name" : "Рівненська область"
    },
    "17" : {
        "name" : "Сумська область"
    },
    "18" : {
        "name" : "Тернопільська область"
    },
    "19" : {
        "name" : "Харківська область"
    },
    "20" : {
        "name" : "Херсонська область"
    },
    "21" : {
        "name" : "Хмельницька область"
    },
    "22" : {
        "name" : "Черкаська область"
    },
    "23" : {
        "name" : "Чернігівська область"
    },
    "24" : {
        "name" : "Чернівецька область"
    },
    "25" : {
        "name" : "Автономна Республіка Крим"
    }
  }

